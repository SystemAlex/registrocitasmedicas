<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->text('body');
            $table->string('name');
            $table->string('firstName');
            $table->bigInteger('CI');
            $table->string('ciudad');
            $table->string('municipio');
            $table->integer('edad');
            $table->date('fecha_nacimiento');
            $table->bigInteger('phone');
            $table->string('genero');
            $table->string('turno');
            $table->time('hora');
            $table->date('fecha_atencion');
            $table->integer('category_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
