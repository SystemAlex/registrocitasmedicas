<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(rand(5,10));
        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'turno'=>$this->faker->jobTitle ,
            'hora'=>$this->faker->time($format = 'H:i:s', $max = 'now'),
            'fecha_atencion'=>$this->faker->date($format = 'Y-m-d', $max = 'now'),
        ];
    }
}
