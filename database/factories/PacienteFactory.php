<?php

namespace Database\Factories;

use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(5,10));
        return [
            'title' => $this->faker->word,
            'name'=>$this->faker->name,
            'firstName'=>$this->faker->firstName,
            'CI'=>$this->faker->ean8,
            'ciudad'=>$this->faker->city,
            'municipio'=>$this->faker->state,
            'edad'=>$this->faker->randomDigit(1,10),
            'direccion'=>$this->faker->address,
            'fecha_nacimiento'=>$this->faker->date($format = 'Y-m-d', $max = 'now'),
            'phone'=>$this->faker->e164PhoneNumber,
            'genero'=>$this->faker->text($maxNbChars = 50)  ,
            'tipoconsulta'=>$this->faker->text($maxNbChars = 50)  ,
            'turno'=>$this->faker->jobTitle ,
            'hora'=>$this->faker->time($format = 'H:i:s', $max = 'now'),
            'fecha_atencion'=>$this->faker->date,
        ];
    }
}
