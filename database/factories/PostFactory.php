<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(5,10));
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'body' => $this->faker->paragraph(rand(200,500)),
            'name'=>$this->faker->name,
            'firstName'=>$this->faker->firstName,
            'CI'=>$this->faker->ean8,
            'ciudad'=>$this->faker->city,
            'municipio'=>$this->faker->state,
            'edad'=>$this->faker->randomDigit(1,10),
            'fecha_nacimiento'=>$this->faker->date($format = 'Y-m-d', $max = 'now'),
            'phone'=>$this->faker->e164PhoneNumber,
            'genero'=>$this->faker->jobTitle,
            'turno'=>$this->faker->jobTitle ,
            'hora'=>$this->faker->time($format = 'H:i:s', $max = 'now') ,
            'fecha_atencion'=>$this->faker->date($format = 'Y-m-d', $max = 'now'),
            'category_id' => $this->faker->randomDigit(2,10),
            'user_id' => $this->faker->randomDigit(2,10),
        ];
    }
}
