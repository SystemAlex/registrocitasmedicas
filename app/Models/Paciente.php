<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'name',
        'firstName',
        'CI',
        'ciudad',
        'municipio',
        'edad',
        'direccion',
        'fecha_nacimiento',
        'phone',
        'genero',
        'tipoconsulta',
        'turno',
        'hora',
        'fecha_atencion',
    ];
}
