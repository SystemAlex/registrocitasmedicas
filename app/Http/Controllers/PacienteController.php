<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Http\Request;

use Illuminate\Support\Str;


class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paciente = Paciente::all();
        
        return response()->json($paciente);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        Paciente::create([
            'title' => $data['title'],
            'name'=>$data['name'],
            'firstName'=>$data['firstName'],
            'CI'=>$data['CI'],
            'ciudad'=>$data['ciudad'],
            'municipio'=>$data['municipio'],
            'edad'=>$data['edad'],
            'direccion'=>$data['direccion'],
            'fecha_nacimiento'=>$data['fecha_nacimiento'],
            'phone'=>$data['phone'],
            'genero'=>$data['genero'],
            'tipoconsulta'=>$data['tipoconsulta'],
            'turno'=>$data['turno'],
            'hora'=>$data['hora'],
            'fecha_atencion'=>$data['fecha_atencion'],
        ]);
        return response()->json(['state'=>'Exito de creacion de publicacion'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paciente $paciente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        //
    }
}
