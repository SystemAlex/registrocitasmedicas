import axios from 'axios'

export default {
    namespaced:true,
    state:{
        allpacientes:[]
    },
    getters:{
        get_all_pacientes(state){
            return state.allpacientes
        }
    },
    mutations:{
        SET_ALL_PACIENTES(state,value){
            state.allpacientes = value
       }
    },
    actions:{
        async getPacientesAll({dispatch}){
            console.log('que dices que queres ver todos lso pacientes')
            await axios.get('/sanctum/csrf-cookie')
            const ct = await axios.get('/api/pacientes')
            return dispatch('mePaciente',ct.data)
        },
       async createPaciente({dispatch},paciente){
            console.log('ohhh nuevo paciente')
            console.log(paciente)
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/pacientes',paciente)
        },
        mePaciente({commit},data){
            console.log(data)
            commit('SET_ALL_PACIENTES',data)
        }
    }   
}
